/*
  VARIABLES
*/

//canvas initialization
var c = document.getElementById("c");
var cc = c.getContext("2d");
c.addEventListener("mousedown", getPosition, false);

//arrays for lines and individual squares
var lines = [0, 100, 200, 300];
var squares = [{x: 0, y: undefined, here: false, hit: false}, {x: 75, y: undefined, here: false, hit: false}, {x: 150, y: undefined, here: false, hit: false}, {x: 225, y: undefined, here: false, hit: false}];
var points = 0;

/*
  MAIN CODE
*/

//draws horizontal lines (these are constant!)
for (i = 0; i < 300; i += 75) {
  cc.moveTo(i, 0);
  cc.lineTo(i, 400);
}

//generates first square
generateSquare();

//draws vertical lines by iterating through array
lines.forEach(function (element, index, array) {
  cc.moveTo(0, element);
  cc.lineTo(300, element);
})

//main interval
setInterval(function () {
  //moves lines and redraws by resetting canvas
  for (i = 0; i < lines.length; i++) {
    c.width = c.width;
    lines[i] += 1;
    if (lines[i] >= 400) {
      lines[i] = 0;
      generateSquare();
      console.log(squares);
    }
  }

  //moves squares
  for (i = 0; i < 4; i++) {
    if (squares[i].y != null) {
      squares[i].y = squares[i].y + 1;
    }
    if (squares[i].y >= 400) {
      if (!squares[i].hit) {
        //gameOver(); buggy!
      }
      squares[i].hit = false;
      squares[i].here = false;
    }
  }

  //redraws horizontal lines since we reset the canvas
  for (i = 0; i < 300; i += 75) {
    cc.moveTo(i, 0);
    cc.lineTo(i, 400);
  }

  //redraws vertical lines
  lines.forEach(function (element, index, array) {
    cc.strokeStyle = "#DDDDDD";
    cc.moveTo(0, element);
    cc.lineTo(300, element);
  })

  //draws squares
  squares.forEach(function (element, index, array) {
    if (squares[index].hit) {
      cc.fillStyle = "#AAAAAA";
      cc.fillRect(element.x, squares[index].y, 75, 100);
      cc.fillStyle = "#000000";
    } else {
      cc.fillRect(element.x, squares[index].y, 75, 100);
    }
  })
  cc.stroke();
}, 1);

/*
  FUNCTIONS
*/

//generates a square
function generateSquare () {
  var id = Math.floor(Math.random() * 4);
  squares[id].y = 0; //should work correctly, right?
  squares[id].here = true; //but it doesn't somehow
}

//gets mouseclick coordinates and clears squares
function getPosition (event) {
  var x = event.x;
  var y = event.y;
  x -= c.offsetLeft;
  y -= c.offsetTop;
  //'hits' squares by iterating through the array
  squares.forEach(function (element, index, array) {
    if (x > element.x && x < element.x + 75) {
      if (y > squares[index].y && y < squares[index].y + 150) {
        squares[index].hit = true;
        points++;
        document.getElementById('points').innerHTML = points;
      } else {
        gameOver();
      }
    }
  })
}

//on game over
function gameOver () {
  document.getElementById('container').innerHTML = "you lost, but scored " + points + " points!";
}

//final draw
cc.stroke();
